/*
    *
    * This file is a part of CoreGarage.
    * A setting manager for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QApplication>

#include <cprime/capplication.h>

#include "coregarage.h"


int main(int argc, char *argv[])
{
    CPrime::CApplication app("CoreGarage", argc, argv);

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName("CoreGarage");
    app.setApplicationVersion(QStringLiteral(VERSION_TEXT));
    app.setDesktopFileName("org.cubocore.CoreGarage.desktop");
    app.setQuitOnLastWindowClosed(true);

    coregarage g;
    QObject::connect(&app, &CPrime::CApplication::messageReceived, [&g]() {
        g.show();
        g.activateWindow();
    });

    if (app.isRunning()) {
		return not app.sendMessage("");
	}

	g.show();

    return app.exec();
}
