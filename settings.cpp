/*
    *
    * This file is a part of CoreGarage.
    * A setting manager for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QFileInfo>
#include <QDir>
#include <QStandardPaths>
#include <QFontDatabase>

#include <cprime/variables.h>
#include <cprime/themefunc.h>
#include <cprime/filefunc.h>

#include <sys/types.h> // getpwuid()
#include <pwd.h>       // getpwuid()
#include <unistd.h>    // getuid()

#include "settings.h"

settings::settings()
{
    defaultSett = QDir(CPrime::Variables::CC_Library_ConfigDir()).filePath("coreapps.conf");
    cSetting = new QSettings(defaultSett, QSettings::NativeFormat);

    // set some default settings that are user specific
    if (!CPrime::FileUtils::exists(defaultSett)) {
        qDebug() << "Settings file " << cSetting->fileName();
        CPrime::FileUtils::setupFolder(CPrime::FolderSetup::ConfigFolder);
    }

    setDefaultSettings(*cSetting);
}

settings::~settings()
{
    delete cSetting;
}

// As default settings not exist we should contain to set value for default
void settings::setDefaultSettings(QSettings& settings)
{
    // C Suite
    if (!settings.contains("CoreApps/KeepActivities")) {
        settings.setValue("CoreApps/KeepActivities", true);}
    if (!settings.contains("CoreApps/EnableExperimental")) {
        settings.setValue("CoreApps/EnableExperimental", false);}
    if (!settings.contains("CoreApps/AutoDetect")) {
        settings.setValue("CoreApps/AutoDetect", true);}
    if (!settings.contains("CoreApps/DisableTrashConfirmationMessage")) {
        settings.setValue("CoreApps/DisableTrashConfirmationMessage", false);}

    if (autoUIMode() == 2) {
        // Check if "CoreApps/IconViewIconSize" exists
        if (!settings.contains("CoreApps/IconViewIconSize")) {
            settings.setValue("CoreApps/IconViewIconSize", QSize(56, 56));}

        // Check if "CoreApps/ListViewIconSize" exists
        if (!settings.contains("CoreApps/ListViewIconSize")) {
            settings.setValue("CoreApps/ListViewIconSize", QSize(48, 48));}

        // Check if "CoreApps/ToolsIconSize" exists
        if (!settings.contains("CoreApps/ToolsIconSize")) {
            settings.setValue("CoreApps/ToolsIconSize", QSize(48, 48));}

    } else {
        // Check if "CoreApps/IconViewIconSize" exists
        if (!settings.contains("CoreApps/IconViewIconSize")) {
            settings.setValue("CoreApps/IconViewIconSize", QSize(48, 48));}

        // Check if "CoreApps/ListViewIconSize" exists
        if (!settings.contains("CoreApps/ListViewIconSize")) {
            settings.setValue("CoreApps/ListViewIconSize", QSize(32, 32));}

        // Check if "CoreApps/ToolsIconSize" exists
        if (!settings.contains("CoreApps/ToolsIconSize")) {
            settings.setValue("CoreApps/ToolsIconSize", QSize(24, 24));}
    }

    // CoreGarage
    if (!settings.contains("CoreGarage/WindowSize")) {
        settings.setValue("CoreGarage/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreGarage/WindowMaximized")) {
        settings.setValue("CoreGarage/WindowMaximized", false);}

    // CoreAction
    if (!settings.contains("CoreAction/PluginsFolders")) {
        settings.setValue("CoreAction/PluginsFolders", {"/usr/lib/coreapps/plugins"});}
    if (!settings.contains("CoreAction/SelectedPlugins")) {
        settings.setValue("CoreAction/SelectedPlugins",
                          QStringList() << "/usr/lib/coreapps/plugins/libbacklight.so"
                                        << "/usr/lib/coreapps/plugins/libcalendar.so"
                                        << "/usr/lib/coreapps/plugins/libnetwork.so"
                                        << "/usr/lib/coreapps/plugins/libsystem.so");}

    // CoreArchiver
    if (!settings.contains("CoreArchiver/WindowSize")) {
        settings.setValue("CoreArchiver/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreArchiver/WindowMaximized")) {
        settings.setValue("CoreArchiver/WindowMaximized", false);}

    // CoreFM
    if (!settings.contains("CoreFM/ViewMode")) {
        settings.setValue("CoreFM/ViewMode", 1);}
    if (!settings.contains("CoreFM/ShowHidden")) {
        settings.setValue("CoreFM/ShowHidden", false);}
    if (!settings.contains("CoreFM/ShowThumb")) {
        settings.setValue("CoreFM/ShowThumb", true);}
    if (!settings.contains("CoreFM/WindowSize")) {
        settings.setValue("CoreFM/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreFM/WindowMaximized")) {
        settings.setValue("CoreFM/WindowMaximized", false);}

    // CoreHunt
    if (!settings.contains("CoreHunt/WindowSize")) {
        settings.setValue("CoreHunt/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreHunt/WindowMaximized")) {
        settings.setValue("CoreHunt/WindowMaximized", false);}

    // CoreImage
    if (!settings.contains("CoreImage/InitialZoomMode")) {
        settings.setValue("CoreImage/InitialZoomMode", false);}
    if (!settings.contains("CoreImage/SortMode")) {
        settings.setValue("CoreImage/SortMode", 4);}
    if (!settings.contains("CoreImage/WindowSize")) {
        settings.setValue("CoreImage/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreImage/WindowMaximized")) {
        settings.setValue("CoreImage/WindowMaximized", false);}

    // CoreKeyboard
    if (!settings.contains("CoreKeyboard/Mode")) {
        settings.setValue("CoreKeyboard/Mode", true);}
    if (!settings.contains("CoreKeyboard/AutoSuggest")) {
        settings.setValue("CoreKeyboard/AutoSuggest", false);}
    if (!settings.contains("CoreKeyboard/DaemonMode")) {
        settings.setValue("CoreKeyboard/DaemonMode", false);}
    if (!settings.contains("CoreKeyboard/KeymapNumber")) {
        settings.setValue("CoreKeyboard/KeymapNumber", 1);}
    if (not settings.contains("CoreKeyboard/Type")) {
        settings.setValue("CoreKeyboard/Type", true);}
    if (not settings.contains("CoreKeyboard/WindowMode")) {
        settings.setValue("CoreKeyboard/WindowMode", false);}
    if (not settings.contains("CoreKeyboard/OpaqueMode")) {
        settings.setValue("CoreKeyboard/OpaqueMode", false);}
    if (!settings.contains("CoreKeyboard/WindowSize")) {
        settings.setValue("CoreKeyboard/WindowSize", QSize(400, 200));}

    // CoreInfo
    if (!settings.contains("CoreInfo/WindowSize")) {
        settings.setValue("CoreInfo/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreInfo/WindowMaximized")) {
        settings.setValue("CoreInfo/WindowMaximized", false);
    }

    // Add system font to CorePad, CoreTerminal
    QFont monoFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    if ( not monoFont.family().length() ) {
        monoFont = QFont("monospace", 9);}
    if (monoFont.styleHint() != QFont::Monospace) {
        monoFont = QFont("monospace", 9);}

    // CorePad
    if (!settings.contains("CorePad/Font")) {
        settings.setValue("CorePad/Font", monoFont);}
    if (!settings.contains("CorePad/WindowSize")) {
        settings.setValue("CorePad/WindowSize", QSize(800, 500));}
    if (!settings.contains("CorePad/WindowMaximized")) {
        settings.setValue("CorePad/WindowMaximized", false);}

    // CorePaint
    if (!settings.contains("CorePaint/WindowSize")) {
        settings.setValue("CorePaint/WindowSize", QSize(800, 500));}
    if (!settings.contains("CorePaint/WindowMaximized")) {
        settings.setValue("CorePaint/WindowMaximized", false);}

    // CorePDF
    if (!settings.contains("CorePDF/Continuous")) {
        settings.setValue("CorePDF/Continuous", true);}
    if (!settings.contains("CorePDF/PageLayout")) {
        settings.setValue("CorePDF/PageLayout", 0);}
    if (!settings.contains("CorePDF/ZoomMode")) {
        settings.setValue("CorePDF/ZoomMode", 1);}
    if (!settings.contains("CorePDF/ZoomFactor")) {
        settings.setValue("CorePDF/ZoomFactor", 100);}
    if (!settings.contains("CorePDF/WindowSize")) {
        settings.setValue("CorePDF/WindowSize", QSize(800, 500));}
    if (!settings.contains("CorePDF/WindowMaximized")) {
        settings.setValue("CorePDF/WindowMaximized", false);}

    // CorePins
    if (!settings.contains("CorePins/WindowSize")) {
        settings.setValue("CorePins/WindowSize", QSize(800, 500));}
    if (!settings.contains("CorePins/WindowMaximized")) {
        settings.setValue("CorePins/WindowMaximized", false);}

    // CoreRenamer
    if (!settings.contains("CoreRenamer/WindowSize")) {
        settings.setValue("CoreRenamer/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreRenamer/WindowMaximized")) {
        settings.setValue("CoreRenamer/WindowMaximized", false);}

    // CoreShot
    if (!settings.contains("CoreShot/WindowSize")) {
        settings.setValue("CoreShot/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreShot/WindowMaximized")) {
        settings.setValue("CoreShot/WindowMaximized", false);}
    if (!settings.contains("CoreShot/AfterShotTaken")) {
        settings.setValue("CoreShot/AfterShotTaken", 2);}
    if (!settings.contains("CoreShot/Delay")) {
        settings.setValue("CoreShot/Delay", 0);}
    if (!settings.contains("CoreShot/SaveLocation")) {
        QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::PicturesLocation) + "/Screenshots");
        cSetting->setValue("CoreShot/SaveLocation", QStandardPaths::writableLocation(QStandardPaths::PicturesLocation) + "/Screenshots");}

    // CoreStats
    if (!settings.contains("CoreStats/WindowSize")) {
        settings.setValue("CoreStats/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreStats/WindowMaximized")) {
        settings.setValue("CoreStats/WindowMaximized", false);}

    // CoreStuff
    QString s = "/usr/share/coreapps/background/default.svg";
    if (!settings.contains("CoreStuff/Background")) {
        settings.setValue("CoreStuff/Background", s);}
    if (!settings.contains("CoreStuff/WallpaperPositon")) {
        settings.setValue("CoreStuff/WallpaperPositon", 1);}

    // CoreTerminal
    struct passwd *pwent;
    pwent = getpwuid(getuid());
    if (!settings.contains("CoreTerminal/Shell")) {
        settings.setValue("CoreTerminal/Shell", QString(pwent->pw_shell));}
    if (!settings.contains("CoreTerminal/Font")) {
        settings.setValue("CoreTerminal/Font", monoFont);}
    if (!settings.contains("CoreTerminal/Opacity")) {
        settings.setValue("CoreTerminal/Opacity", 100);}
    if (!settings.contains("CoreTerminal/HistorySize")) {
        settings.setValue("CoreTerminal/HistorySize", 500);}
    if (!settings.contains("CoreTerminal/KeyTab")) {
        settings.setValue("CoreTerminal/KeyTab", "default");}
    if (!settings.contains("CoreTerminal/CursorShape")) {
        settings.setValue("CoreTerminal/CursorShape", 0);}
    if (!settings.contains("CoreTerminal/ColorScheme")) {
        settings.setValue("CoreTerminal/ColorScheme", "WhiteOnBlack");}
    if (!settings.contains("CoreTerminal/TERM")) {
        settings.setValue("CoreTerminal/TERM", "xterm-256color");}
    if (!settings.contains("CoreTerminal/Rows")) {
        settings.setValue("CoreTerminal/Rows", 30);}
    if (!settings.contains("CoreTerminal/Columns")) {
        settings.setValue("CoreTerminal/Columns", 120);}

    // CoreTime
    if (!settings.contains("CoreTime/WindowSize")) {
        settings.setValue("CoreTime/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreTime/WindowMaximized")) {
        settings.setValue("CoreTime/WindowMaximized", false);}

    // CoreUniverse
    if (!settings.contains("CoreUniverse/WindowSize")) {
        settings.setValue("CoreUniverse/WindowSize", QSize(800, 500));}
    if (!settings.contains("CoreUniverse/WindowMaximized")) {
        settings.setValue("CoreUniverse/WindowMaximized", false);}

    cSetting->sync();
}

int settings::autoUIMode() const
{
    if (CPrime::ThemeFunc::getFormFactor() == CPrime::FormFactor::Mobile) {
        return 2; // Mobile
    } else if (CPrime::ThemeFunc::getFormFactor() == CPrime::FormFactor::Tablet &&  CPrime::ThemeFunc::getTouchMode() == true) {
        return 1; // Tablet
    } else {
        return 0; // Desktop
    }
}

settings::cProxy settings::getValue(const QString &appName, const QString &key,
                                    const QVariant &defaultValue)
{
    if (appName == "CoreApps" && key == "UIMode") { // Wants to get CoreApps/UIMode
        // Check whether CoreApps/AutoDetect is On
		bool isAutoDetect = cSetting->value("CoreApps/AutoDetect").toBool();

        if (isAutoDetect)
            return cProxy { cSetting, "Dummy", autoUIMode() };
    }

    return cProxy{ cSetting, appName + "/" + key, defaultValue };
}

void settings::setValue(const QString &appName, const QString &key, QVariant value)
{
	cSetting->setValue(appName + "/" + key, value);
}

QString settings::defaultSettingsFilePath() const
{
	return cSetting->fileName();
}
