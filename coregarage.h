/*
    *
    * This file is a part of CoreGarage.
    * A setting manager for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>
#include <QToolButton>

#include <cprime/variables.h>

#include "settings.h"

namespace Ui {
    class coregarage;
}

class coregarage : public QWidget {
    Q_OBJECT

public:
    explicit coregarage(QWidget *parent = nullptr);
    ~coregarage();

private slots:
    void on_cancel_clicked();
    void on_ok_clicked();
    void pageClick(QToolButton *btn, int i, const QString& title);
    void on_general_clicked();
    void on_coreaction_clicked();
    void on_backUp_clicked();
    void on_restore_clicked();
    void on_setTerminals_clicked();
    void on_setFileManger_clicked();
    void on_setImageEditor_clicked();
    void showBackupMessage();
    void showRestoreMessage();
    void on_coreterminal_clicked();
    void showSideView();

    void on_addPluginPath_clicked();
    void on_delPluginPath_clicked();
    void on_movePluginUp_clicked();
    void on_movePluginDown_clicked();

    void on_setMetadataViewer_clicked();
    void on_setSearchApp_clicked();

    void on_terminalFont_clicked();
    void on_others_clicked();
	void on_setBG_clicked();
    void on_restoreDefault_clicked();
    void on_setRenamerApp_clicked();
    void on_help_clicked();
    void on_appTitle_clicked();
    void on_menu_clicked(bool checked);
    void on_autoDetect_clicked(bool checked);

private:
    Ui::coregarage *ui;
    settings  *smi;
    bool            autoDetect, windowMaximized;
    int             uiMode;
    QSize           toolsIconSize, listViewIconSize, windowSize;
    QString         m_defaultPluginPath = "/usr/lib/coreapps/plugins";
    QString         CoreApps_ConfigDir = CPrime::Variables::CC_System_ConfigDir() + "coreapps";

	typedef struct plugin_info_t {
		QString name;
		QString version;
	} PluginInfo;

	QStringList m_pluginsFolders;
	QMap<QString, PluginInfo> allPlugins;

    void initialize();
    void setupCoreAppsPage();
    void setupCoreActionPage();
    void setupCoreTerminalPage();
    void setupCoreStuffPage();
    void loadSettings();
    void startSetup();
    void saveSettings();
	void loadPlugins();
    void loadPluginsPaths();
    void sortCheckedAtFirst();
    void setupIcons();
};
